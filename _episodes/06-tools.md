---
title: "Using Tools to Manage Docker Containers"
teaching: -1
exercises: -1
questions:
- "What tools can help me handle containers?"
objectives:
- "Start tool(s) and get an overview"
keypoints:
- "Tools which help to understand complex systems"
---

{% include links.md %}

## Why Should I Use Additional Tools?

The more complex the systems we build become, the harder it gets to handle them.
Additionally, containers running on a computer are not easily accessible to get their content or change values.
Because of these issues, smart people build tools to help with that.
We will have a look at certain tools, start them and get to see their basic functionality.

## Portainer

[Portainer](https://www.portainer.io/) is a web-based tool, designed to handle the Docker client with a graphical interface.
It is an open source, free software with additional paid features.
The free version (community edition) is sufficient for most of scientific needs.

### Installation

There are multiple ways to run Portainer.
Luckily, it comes packed within its own Docker image, which we can start with the previously learned commands.

~~~
docker volume create portainer_data

docker run -d \
       -p 8000:8000 \
       -p 9000:9000 \
       --name=portainer \
       --restart=always \
       -v /var/run/docker.sock:/var/run/docker.sock \
       -v portainer_data:/data \
       portainer/portainer-ce
~~~
{: .language-terminal}

```yaml
version: "3.9"

services:
  portainer:
    image: portainer/portainer-ce
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data
    restart: always
    ports:
    - 8000:8000
    - 9000:9000

volumes:
  portainer_data: {}
```

> **Note:** For Portainer to run, it requires access to the Docker socket.
> This is an advanced feature, which is out of the scope of this episode.
> But be aware, that this might lead to security issues, if you do that for untrusted applications.

After the container is started, you can open your web-browser and access Portainer via [localhost:9000](localhost:9000).
If you start Portainer for the first time, you are asked to create a local account.

![Portainer account creation](../fig/portainer_create_account.PNG)

> **Note:** Disable collection of anonymous data

When you are asked to choose the container engine, select Docker.
The next page, you are seeing, is the homepage, where we need to choose an endpoint.
Here you could handle even remote hosts, but we will use our local endpoint.

![Portainer select endpoint](../fig/portainer_select_endpoint.PNG)

### Basic Usage

If we successfully connected Portainer to our local Docker engine, we are now able to manage Docker via Portainer's web-interface.
For now, we will only get a brief overview about the Docker features we learned so far.

To get an overview of all containers on your machine, click on the menu entry `Containers` in the left sidebar.
Every container has a checkbox to perform actions like start, stop and kill and quick actions (Logs, Console, Inspect, Stats).

> **Instructor Note:** give a brief overview about these features.

Overview of Portainers container list with the most common actions.

![Portainer container overview](../fig/portainer_container_overview.PNG)

There are containers that will log a lot.
Sometimes too much to properly view them in a terminal.
This is where Portainers log-view comes in handy.

![Portainer account creation](../fig/portainer_logs.PNG)

Portainer also offers the possibility to attach a shell to a container.

![Portainer account creation](../fig/portainer_console.PNG)

> **Note:** Most containers do not have a `bash` installed and use a `sh` instead.
>           Sometimes containers have no shell at all (like the Portainer container itself).

### Shut Down Portainer

To shut down Portainer - because it runs within a docker container itself - we will use Portainer.
Click on Containers in the left sidebar.
Select the Portainer container and click `Remove` at the top of the list.
The Portainer web-interface will throw errors, which is normal, because we shut down the application inside the application itself.

> **Note:** The option to remove non-persistent volumes  when removing a container will remove all **unnamed** volumes associated with the container you are removing.
>           The previously created `portainer_data` will not be deleted.
